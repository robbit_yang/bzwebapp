# -*- coding:utf-8 -*-

"""
打印共享对象
"""
from PyQt5.QtCore import QObject,pyqtProperty,Qt
from PyQt5.QtGui import QTextDocument,QPixmap
from PyQt5.QAxContainer import QAxWidget
import platform,os
from PyQt5.QtWidgets import QMessageBox,QPushButton,qApp,QSplashScreen


class ActiveXExtend(QObject):
    def __init__(self,view):
        super().__init__()
        self.view = view
        self.ocx = QAxWidget("2105C259-1E0C-4534-8141-A753534CB4CA")

    def _OnReceiveMsg(self):
        pass


class Bzprintshareobj(QObject):
    def __init__(self):
        super(Bzprintshareobj,self).__init__(None)
        self.printhtml = ''
        self.sysstr = platform.system()
        if self.sysstr == 'Windows':
            self.act = ActiveXExtend(None)
        else:
            self.act = None

    def _getStrValue(self):
        printers = self.act.ocx.GET_PRINTER_COUNT()
        # print(printers)
        return str(printers)

    def _setStrValue(self,str):
        str_list = str.split('=====',2)

        if self.sysstr == 'Windows':
            try:
                splash = QSplashScreen(QPixmap(os.path.join('images', "loading.png")))
                splash.showMessage("加载中... ", Qt.AlignHCenter | Qt.AlignBottom, Qt.black)
                splash.show()  # 显示启动界面
                self.act.ocx.PRINT_INIT('博智打印')
                if str_list[0] == '001':
                    self.act.ocx.ADD_PRINT_TABLE(20, 0, 200, 1000, str_list[1])
                elif str_list[0] == '003':
                    self.act.ocx.ADD_PRINT_TABLE(6, 20, 770, 1000, str_list[1])
                elif str_list[0] == '004':
                    self.act.ocx.ADD_PRINT_TABLE(6, 20, 770, 1000, str_list[1])
                elif str_list[0] == 'bmymb58':
                    self.act.ocx.ADD_PRINT_TABLE(0, 1, 180, 1000, str_list[1])
                elif str_list[0] == 'bmymb80':
                    self.act.ocx.ADD_PRINT_TABLE(0, 1, 260, 1000, str_list[1])
                elif str_list[0] == 'bmycz58':
                    self.act.ocx.ADD_PRINT_TABLE(0, 1, 180, 1000, str_list[1])
                    self.act.ocx.SET_PRINT_STYLEA(0,'LineSpacing',0)
                    self.act.ocx.SET_PRINT_STYLEA(0,'TableHeightScope',1)
                elif str_list[0] == 'bmycz80':
                    self.act.ocx.ADD_PRINT_TABLE(0, 1, 238, 1000, str_list[1])
                    self.act.ocx.SET_PRINT_STYLEA(0, 'LineSpacing', 0)
                    self.act.ocx.SET_PRINT_STYLEA(0, 'TableHeightScope', 1)
                ret = self.act.ocx.PREVIEW()
                splash.finish(qApp.activeWindow())
                if int(ret) == 0:
                    msgbox = QMessageBox()
                    msgbox.setText('你放弃了打印...')
                    msgbox.exec()

            except Exception as e:
                    QMessageBox.critical(self, '系统错误', '调用打印组件加载异常，请联系开发商 13973343798 言工', QMessageBox.Yes,
                                         QMessageBox.Yes)
        else:
            QMessageBox.critical(self, '系统错误', '没有 ' + str(self.sysstr) + ' 打印驱动 请咨询13973343798 言工', QMessageBox.Yes,
                                 QMessageBox.Yes)

    # 执行测试打印
    def printHtml(self, printer):
        textDocument = QTextDocument()
        textDocument.setHtml(self.printhtml)
        textDocument.print(printer)

    # 需要定义对外发布的方法
    strValue = pyqtProperty(str, fget=_getStrValue, fset=_setStrValue)