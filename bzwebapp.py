# -*- coding:utf-8 -*-

import sys

from PyQt5.QtCore import QCoreApplication,Qt
from PyQt5.QtGui import QPixmap
from PyQt5.QtWidgets import QApplication,QPushButton,qApp,QSplashScreen,QMessageBox
from PyQt5.QtWidgets import QWidget
from web.bzweb import WebPage,Bzmainwindow
from PyQt5.QtWebChannel import  QWebChannel
from bzprinter.bzprintshareobj import Bzprintshareobj
from multiprocessing import freeze_support
import pickle
import os
import time

exefilename = "execute.pkl"
def is_exec_app():
    if not os.path.exists(exefilename):
        pickle.dump([True],open(exefilename,mode='wb'))
        return True
    else:
        return False

"""
入口主函数
"""
if __name__ == '__main__':
    # freeze_support()
    # 应用层
    app = QApplication(sys.argv)
    if is_exec_app() == True:
        # app = QApplication(sys.argv)
        # 显示启动界面
        splash = QSplashScreen(QPixmap(os.path.join('images', "loading.png")))
        splash.showMessage("加载中... ", Qt.AlignHCenter | Qt.AlignBottom, Qt.black)
        splash.show()

        bzwindow = Bzmainwindow()
        # 允许主进程处理事件
        qApp.processEvents()

        # 注册打印
        channel = QWebChannel()
        printObj = Bzprintshareobj()
        channel.registerObject("bz_print_bridge", printObj)

        #bzwindow.page.setWebChannel(channel)
        bzwindow.webView.page().setWebChannel(channel)
        bzwindow.show()

        splash.finish(bzwindow)

        sys.exit(app.exec_())
    else:
        reply = QMessageBox.warning(qApp.activeWindow(),'系统提示','不能重复启动,是否删除启动标示？',
                            QMessageBox.Yes | QMessageBox.No,
                            QMessageBox.No)
        if reply == QMessageBox.Yes:
            os.remove("execute.pkl")
            QMessageBox.about(qApp.activeWindow(),'系统提示','请重新启动软件')
        else:
            qApp.activePopupWidget()
